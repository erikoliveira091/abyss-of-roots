using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour
{
    [Header("Ink JSON")]
    public TextAsset[] inkJSON;

    private Story currentStory;

    private Dialogue dialogue;

    [SerializeField] TextMeshProUGUI text;

    public int index;

    public int dialogueNumber;

    private static DialogueManager instance;

    public bool inDialog;

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("Found more than one Dialogue Manager in the scene");
        }
        instance = this;

        dialogue = GameObject.FindGameObjectWithTag("DialoguePanel").GetComponent<Dialogue>();
    }

    public static DialogueManager GetInstance()
    {
        return instance;
    }

    public void EnterDialogueMode(int assetsNumber)
    {
        if(dialogueNumber == 6)
        {
            return;
        }

        if(inDialog)
        {
            return;
        }

        dialogueNumber = assetsNumber;

        if(assetsNumber == 5 || assetsNumber == 6)
        {
            text.color = Color.red;

        }

        StopAllCoroutines();

        for (int i = 0; i < dialogue.lines.Length - 1; i++)
        {
            dialogue.lines[i] = null;
        }
        currentStory = new Story(inkJSON[assetsNumber].text);
        index = 0;
        StartCoroutine(ContinueStory());
        inDialog = true;
    }

    private IEnumerator ExitDialogueMode()
    {
        yield return new WaitForSeconds(0.2f);
    }

    IEnumerator ContinueStory()
    {
        if (currentStory.canContinue)
        {
            dialogue.lines[index] = currentStory.Continue();
            dialogue.emptyText--;
            index++;
            yield return new WaitForSeconds(0.001f);
            StartCoroutine(ContinueStory());

            //dialogueText.text = currentStory.Continue();
            // op��es de exibi��o, se houver, para esta linha de di�logo
        }
        else
        {
            dialogue.textComponent.text = null;
            dialogue.StartDialogue();
            StartCoroutine(ExitDialogueMode());
        }
    }

    
}
