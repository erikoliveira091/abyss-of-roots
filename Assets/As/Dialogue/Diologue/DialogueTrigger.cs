using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    
    [SerializeField] private int assetsNumber;
    [SerializeField] private GameObject rootPrefab;
    [SerializeField] private Transform[] spawns;

    public bool playerInRange;

    public bool dialogueRead;

    private AudioSource sound;

    private void Awake()
    {
        playerInRange = false;
        
        if(gameObject.CompareTag("Barrier"))
        {
            sound = GetComponent<AudioSource>();
        }
    }

    private void Update()
    {
        if(playerInRange)
        {
            if(!dialogueRead)
            {
                dialogueRead = true;
                DialogueManager.GetInstance().EnterDialogueMode(assetsNumber); 

                if(assetsNumber == 5)
                {
                    for (int i = 0; i < spawns.Length - 1; i++)
                    {
                        Instantiate(rootPrefab, spawns[i].position, Quaternion.identity);
                    }
                }
                
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Barrier"))
            {
                sound.Play();
            }
                playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = false;
        }
    }

}
