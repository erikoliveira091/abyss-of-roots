using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI.Table;

public class TentacleExit : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float speed;
    [SerializeField] private float rotSpeed;
    [SerializeField] private float distance;

    AudioSource attack;

    private Vector3 pos;

    private Quaternion rot;

    private bool isStarted;

    void Start()
    {
        attack = GetComponent<AudioSource>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        pos = transform.position - target.position;
        rot = Quaternion.LookRotation(pos);

        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, target.position) > distance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        else
        {
            target.transform.parent = transform.transform;

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y - 1000, transform.position.z), speed * Time.deltaTime);

            if(!isStarted)
            {
                isStarted = true;
                StartCoroutine(GameOver());
            }
            
        }
    }

    IEnumerator GameOver()
    {
        attack.Play();
        yield return new WaitForSeconds(5f);
        GameManager.GetInstance().GameOver();
        target.transform.parent = null;

        Destroy(gameObject);
    }
}
