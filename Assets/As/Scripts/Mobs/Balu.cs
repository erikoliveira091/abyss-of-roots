using UnityEngine;

public class Balu : MonoBehaviour
{
    public float speed;
    public float step;
    public Transform target;
    public Transform target2;
    public bool isMove;
    Vector3 pos;

    private AudioSource sound;

    Rigidbody rb;
    Quaternion rot;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();

    }

    private void Update()
    {
        pos = transform.position - target.position;

        rot = Quaternion.LookRotation(pos);

        transform.rotation = Quaternion.Lerp(transform.rotation, rot, 5f * Time.deltaTime);

        if (Vector3.Distance(target.position, transform.position) > 5)
        {
            if (isMove)
            {
                Move();
            }
        }
        

        
    }

    public void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        if(Vector3.Distance(target.position, transform.position) < 10)
        {
            target = target2;
            rot = Quaternion.LookRotation(target.position);
            GetComponent<AudioSource>().Play();
        }
    }

    public void ISMOVE()
    {
        isMove = true;
    }

}
