using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FishController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float idleTime;

    [SerializeField] private Transform[] patrolPoints;
    [SerializeField] private int index;

    private bool isIdle;

    private Vector3 pos;

    private Quaternion rot;


    void Start()
    {
        
    }

    void Update()
    {
        pos = transform.position - patrolPoints[index].position;

        rot = Quaternion.LookRotation(pos);

        transform.rotation = Quaternion.Lerp(transform.rotation, rot, 5f * Time.deltaTime);

        if (Vector3.Distance(transform.position, patrolPoints[index].position) > 1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, patrolPoints[index].position, speed * Time.deltaTime);
        }
        else
        {
            if(!isIdle)
            {
                isIdle = true;
                StartCoroutine(Patrol());
            }
        }
    }

    IEnumerator Patrol()
    {
        if(index < patrolPoints.Length - 1)
        {
            index++;
        }
        else
        {
            index = 0;
        }
        
        yield return new WaitForSeconds(idleTime);
        isIdle = false;
    }


}
