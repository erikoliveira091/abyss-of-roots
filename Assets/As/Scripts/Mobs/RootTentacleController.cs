using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RootTentacleController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform head;
    [SerializeField] private float speed;
    [SerializeField] private float rotSpeed;
    [SerializeField] private float distance;
    private Vector3 pos;
    private Animator anim;

    private Quaternion rot;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        pos = transform.position - target.position;

        rot = Quaternion.LookRotation(pos);

        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotSpeed * Time.deltaTime);
        
        if (Vector3.Distance(transform.position, target.position) > distance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.GetInstance().GameOver();
        }
    }


}
