using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public int dialogueIndex;

    public int fishCollected;

    public int rootCollected;

    private bool notRepeat;

    public bool gameOver;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Found more than one Dialogue Manager in the scene");
        }
        instance = this;
    }

    public static GameManager GetInstance()
    {
        return instance;
    }

    void Update()
    {
        if(fishCollected == 1 && !notRepeat)
        {
            notRepeat = true;
            DialogueManager.GetInstance().EnterDialogueMode(2);
        }
    }

    public void GameOver()
    {
        gameOver = true;
        DialogueManager.GetInstance().EnterDialogueMode(6);
        StartCoroutine(Quit());
    }

    IEnumerator Quit()
    {
        yield return new WaitForSeconds(20f);
        SceneManager.LoadScene("Menu");
    }
}
