using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetFish : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float curSpeed;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.AddForce(-transform.up * curSpeed);
    }
}
