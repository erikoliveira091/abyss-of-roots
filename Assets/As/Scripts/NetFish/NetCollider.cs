using UnityEngine;

public class NetCollider : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fish"))
        {
            GameManager.GetInstance().fishCollected++;
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

}
