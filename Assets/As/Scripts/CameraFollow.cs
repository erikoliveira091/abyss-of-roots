using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

public class CameraFollow : MonoBehaviour
{
    public Transform currTarget;
    public Transform target;
    public Transform targetAlternative;
    public float movSmoothing;
    public float rotSmoothing;

    public SubmarineController submarineController;

    private bool camAlt;

    private bool canCamAlt;

    [SerializeField] private Volume volume;

    [SerializeField] private VolumeProfile[] profiles;

    void Start()
    {
        volume.profile = profiles[0]; 
        canCamAlt = true;
        currTarget = target;
        transform.position = new Vector3(currTarget.position.x, currTarget.position.y, currTarget.position.z);
        transform.rotation = currTarget.rotation;
    }

    private void Update()
    {

        if (Input.GetKey(KeyCode.LeftAlt))
        {
            if(canCamAlt)
            {
                canCamAlt = false;
                StartCoroutine(CamAlt());
            }
        }

        if(!GameManager.GetInstance().gameOver)
        {
            if (camAlt)
            {
                volume.profile = profiles[1];
                currTarget = targetAlternative;
            }
            else
            {
                volume.profile = profiles[0];
                currTarget = target;
            }
        }
        else
        {
            volume.profile = profiles[2];
        }
    }

    void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(currTarget.position.x, currTarget.position.y, currTarget.position.z), movSmoothing);
        transform.rotation = Quaternion.Slerp(transform.rotation, currTarget.rotation, rotSmoothing);
    }

    IEnumerator CamAlt()
    {
        camAlt = !camAlt;
        yield return new WaitForSeconds(1f);
        canCamAlt = true;
    }
}
