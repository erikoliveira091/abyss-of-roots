using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textComponent;
    public string[] lines = new string[4];
    public float textSpeed;

    public float waitTime;

    public int index;

    public int emptyText;

    public bool inDialogue;

    void Start()
    {
        emptyText = 4;
        textComponent.text = null;
        StartDialogue();
    }

    void Update()
    {
        
    }

    public void StartDialogue()
    {
        index = 0;
        StartCoroutine(TypeLine());
    }

    IEnumerator TypeLine()
    {
        inDialogue = true;
        foreach (char c in lines[index].ToCharArray())
        {
            textComponent.text += c;
            yield return new WaitForSeconds(textSpeed);

            if (textComponent.text == lines[index])
            {
                StartCoroutine(Wait());

            }
            
        }

    }

    void NextLine()
    {
        
        if (index < ((lines.Length - 1) - emptyText))
        {
            index++;         
            textComponent.text = null;
            StartCoroutine(TypeLine());
        }
        else
        {
            if(!inDialogue)
            {
                DialogueManager.GetInstance().inDialog = false;
                emptyText = 4;
                textComponent.text = null;
            }
            
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);
        inDialogue = false;
        NextLine();
    }
}
