using Mono.Cecil.Cil;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class SubmarineController : MonoBehaviour
{
    public float speedChangeAmount;
    public float maxForwardSpeed;
    public float maxBackwardSpeed;
    public float minSpeed;
    public float turnSpeed;
    public float riseSpeed;
    public float stabilizationSmoothing;

    private float curSpeed;
    public bool stop;

    private bool canUseNF;
    [SerializeField] private GameObject netPrefab;
    [SerializeField] private Transform netPivot;

    AudioSource soundMove;

    private Rigidbody rb;
    private SubmarineAnimations submarineAnimations;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        submarineAnimations = GetComponent<SubmarineAnimations>();
        soundMove = GetComponent<AudioSource>();
        stop = false;
        canUseNF = true;
    }

    void FixedUpdate()
    {
        Move();
        Rise();
        Turn();
        Stabilize();
        NetFishUse();

        if (Input.GetKey(KeyCode.Space))
        {
            Stop();
        }

        if(!soundMove.isPlaying && curSpeed != 0)
        {
            soundMove.Play();
        }
    }

    void Stop()
    {
        stop = true;
        curSpeed = 0;
        soundMove.Stop();
    }

    void Move()
    {
        if (Input.GetKey(KeyCode.W) && !stop)
        {
            curSpeed += speedChangeAmount;
            submarineAnimations.Spin(1);

        }
        else if (Input.GetKey(KeyCode.S) && !stop)
        {
            
            submarineAnimations.Spin(-1);
            curSpeed -= speedChangeAmount;

        }
        else if (Mathf.Abs(curSpeed) <= minSpeed)
        {
            curSpeed = 0;
            submarineAnimations.Spin(0);

            stop = false;
        }
        else if(curSpeed != 0  && !stop)
        {
            submarineAnimations.Spin(curSpeed/Mathf.Abs(curSpeed) / 2);
        }

        curSpeed = Mathf.Clamp(curSpeed, -maxBackwardSpeed, maxForwardSpeed);
        rb.AddForce(transform.forward * curSpeed);
    }

    void Turn()
    {
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddTorque(transform.up * turnSpeed);
            submarineAnimations.SpinVol(-1);

        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.AddTorque(transform.up * -turnSpeed);
            submarineAnimations.SpinVol(1);

        }
        else
        {
            submarineAnimations.SpinVol(0);
        }
    }

    void Rise()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            rb.AddForce(transform.up * riseSpeed);
        }
        else if (Input.GetKey(KeyCode.LeftControl))
        {
            rb.AddForce(transform.up * -riseSpeed);
        }
    }

    void Stabilize()
    {
        rb.MoveRotation(Quaternion.Slerp(rb.rotation, Quaternion.Euler(new Vector3(0, rb.rotation.eulerAngles.y, 0)), stabilizationSmoothing));
    }

    void NetFishUse()
    {
        if(Input.GetKey(KeyCode.E) && canUseNF)
        {
            canUseNF = false;
            StartCoroutine(NFUse());
        }
    }

    IEnumerator NFUse()
    {
        GameObject net = Instantiate(netPrefab, netPivot.position, netPivot.rotation);
        yield return new WaitForSeconds(5f);
        Destroy(net);
        canUseNF = true;
    }


}
