using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubmarineAnimations : MonoBehaviour
{
    public float turbineSpinSpeed;
    public float turbineSmoothing;
    private float lastTurbineTurn;

    public float volSpinSpeed;
    public float volSmoothing;
    private float lastVolTurn;

    public Transform turbine;

    public Transform volant;

    public void Spin(float dir)
    {
        float curTurn = Mathf.MoveTowards(lastTurbineTurn, turbineSpinSpeed * dir, turbineSmoothing * Time.deltaTime);
        turbine.Rotate(new Vector3(0, 0, curTurn));
        lastTurbineTurn = curTurn;
    }

    public void SpinVol(float dir)
    {
        float curTulo = Mathf.MoveTowards(lastVolTurn, volSpinSpeed * dir, volSmoothing * Time.deltaTime);
        volant.Rotate(new Vector3(0, 0, curTulo));
        lastVolTurn = curTulo;
    }
}
