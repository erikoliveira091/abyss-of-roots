using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnterCollision : MonoBehaviour
{
    public UnityEvent ativar;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            ativar.Invoke();
            Debug.Log("Ativou");
        }
        
    }

}
