using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerExitMap : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    Transform pos;

    private bool canSpawn = true;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            pos = other.transform;

            if(canSpawn)
            {
                canSpawn = false;
                StartCoroutine(SpawnTentacle());
            }
        }
    }
    IEnumerator SpawnTentacle()
    {
        Instantiate(prefab, pos.position - new Vector3(0, 80, 0), Quaternion.identity);
        yield return new WaitForSeconds(20f);
        canSpawn = true;
    }
}
