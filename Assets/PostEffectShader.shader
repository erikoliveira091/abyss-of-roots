﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PostEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Height("Height", Range(0,500)) = 1
		_Length("Amount", Range(0,500)) = 1
		_Speed("Speed", Range(0,100)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float _Speed,_Length,_Height;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv + float2(0,sin(i.vertex.x/_Length + _Time.x *_Speed)/ _Height));
				// just invert the colors
				//col = 1 - col;
				return col;
			}
			ENDCG
		}
	}
}
