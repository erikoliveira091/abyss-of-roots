using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndMap : MonoBehaviour
{
    [SerializeField] Transform spawn;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            collision.transform.position = spawn.position;
        }
        
    }
}
